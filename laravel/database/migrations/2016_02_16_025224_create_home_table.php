<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_principal');
            $table->string('numero1');
            $table->string('numero1_titulo');
            $table->string('numero1_frase');
            $table->string('numero2');
            $table->string('numero2_titulo');
            $table->string('numero2_frase');
            $table->string('numero3');
            $table->string('numero3_titulo');
            $table->string('numero3_frase');
            $table->string('numero4');
            $table->string('numero4_titulo');
            $table->string('numero4_frase');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('home');
    }
}
