<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lutador extends Model
{
    protected $table = 'lutadores';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
