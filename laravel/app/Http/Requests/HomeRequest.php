<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'texto_principal' => 'required',
            'numero1'         => 'required',
            'numero1_titulo'  => 'required',
            'numero1_frase'   => 'required',
            'numero2'         => 'required',
            'numero2_titulo'  => 'required',
            'numero2_frase'   => 'required',
            'numero3'         => 'required',
            'numero3_titulo'  => 'required',
            'numero3_frase'   => 'required',
            'numero4'         => 'required',
            'numero4_titulo'  => 'required',
            'numero4_frase'   => 'required',
        ];
    }
}
