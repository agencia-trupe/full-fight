<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{

    public function index()
    {
        return view('frontend.news.index');
    }

    public function show()
    {
        return view('frontend.news.show');
    }

}
