<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\GaleriaRequest;
use App\Http\Controllers\Controller;

use App\Models\Galeria;

use App\Helpers\CropImage;

class GaleriaController extends Controller
{
    private $image_config = [
        [
            'width'  => 293,
            'height' => 353,
            'path'   => 'assets/img/galeria/thumbs/'
        ],
        [
            'width'  => '980',
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/galeria/'
        ]
    ];

    public function index()
    {
        $imagens = Galeria::ordenados()->get();

        return view('painel.galeria.index', compact('imagens'));
    }

    public function show(Galeria $imagem)
    {
        return $imagem;
    }

    public function store(GaleriaRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = Galeria::create($input);
            $view = view('painel.galeria.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Galeria $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.galeria.index')
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
