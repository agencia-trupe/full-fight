<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\HomeRequest;

use App\Http\Controllers\Controller;
use App\Models\Home;

class HomePageController extends Controller
{
    public function index()
    {
        $home = Home::first();

        return view('painel.home.index', compact('home'));
    }

    public function update(HomeRequest $request, Home $home)
    {
        try {

            $input = $request->all();

            $home->update($input);
            return redirect()->route('painel.home.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
