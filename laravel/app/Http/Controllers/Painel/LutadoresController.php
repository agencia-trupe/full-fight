<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\LutadorRequest;
use App\Http\Controllers\Controller;

use App\Models\Lutador;
use App\Helpers\CropImage;

class LutadoresController extends Controller
{
    private $image_config = [
        'width'  => 270,
        'height' => 270,
        'path'   => 'assets/img/lutadores/'
    ];

    public function index()
    {
        $lutadores = Lutador::ordenados()->get();

        return view('painel.lutadores.index', compact('lutadores'));
    }

    public function create()
    {
        return view('painel.lutadores.create');
    }

    public function store(LutadorRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Lutador::create($input);
            return redirect()->route('painel.lutadores.index')->with('success', 'Lutador adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar lutador: '.$e->getMessage()]);

        }
    }

    public function edit(Lutador $lutador)
    {
        return view('painel.lutadores.edit', compact('lutador'));
    }

    public function update(LutadorRequest $request, Lutador $lutador)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $lutador->update($input);
            return redirect()->route('painel.lutadores.index')->with('success', 'Lutador alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar lutador: '.$e->getMessage()]);

        }
    }

    public function destroy(Lutador $lutador)
    {
        try {

            $lutador->delete();
            return redirect()->route('painel.lutadores.index')->with('success', 'Lutador excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir lutador: '.$e->getMessage()]);

        }
    }
}
