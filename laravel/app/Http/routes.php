<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('academia', ['as' => 'academia', 'uses' => 'AcademiaController@index']);
Route::get('modalidades', ['as' => 'modalidades', 'uses' => 'ModalidadesController@index']);
Route::get('news/exemplo', ['as' => 'news.show', 'uses' => 'NewsController@show']);
Route::get('news', ['as' => 'news', 'uses' => 'NewsController@index']);
Route::get('galeria', ['as' => 'galeria', 'uses' => 'GaleriaController@index']);
Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);


// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);
    Route::resource('home', 'HomePageController');
    Route::resource('banners', 'BannersController');
    Route::resource('lutadores', 'LutadoresController');
    Route::resource('galeria', 'GaleriaController');
    Route::resource('contato/recebidos', 'ContatosRecebidosController');
    Route::resource('contato', 'ContatoController');
    Route::resource('usuarios', 'UsuariosController');
    Route::post('order', 'HomeController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});
