@extends('frontend.common.template')

@section('content')

<main>

    <section class="well mobile_cntr">
        <div class="container">

            <h2>
                <span>Maecenas sagittis dignissim pharetra</span>
                A FEW WORDS ABOUT US
            </h2>
            <div class="row marTop2">
                <div class="col-lg-6 col-md-6 col-sm-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page2_pic1.jpg') }}" alt="">
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6  wow fadeIn">
                    <h4>Lorem ipsum dolor sit amet conse ctetur adipisi</h4>
                    <p>Duis ac turpis. Integer rutrum ante eu lacus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aennummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis oque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at.<br><br>Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. Lorem ipsum dolor sit amet, consectetue.</p>
                </div>
            </div>

            <hr>

            <h2>
                <span>nteger ut lacus sit amen</span>
                OUR ADVANTAGES
            </h2>

            <ul class="list3">
                <li class="col-md-4 col-sm-4 col-xs-12  wow fadeIn material-icons-access_alarms">
                    <h4>Lorem ipsum dolor sit </h4>
                    <p>Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris ferme ntum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor.</p>
                </li>

                <li class="col-md-4 col-sm-4 col-xs-12  wow fadeIn material-icons-card_giftcard">
                    <h4>Maecenas tristique orci</h4>
                    <p>Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris ferme ntum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor.</p>
                </li>

                <li class="col-md-4 col-sm-4 col-xs-12  wow fadeIn material-icons-directions_run">
                    <h4>Cum sociis natoque</h4>
                    <p>Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris ferme ntum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor.</p>
                </li>
            </ul>

            <hr>

            <h2>
                <span>Praesent vestibulum molestie</span>
                Our Instructors
            </h2>

            <div class="row marTop1 mobile_cntr">
                <div class="col-md-3 col-sm-3 col-xs-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page2_pic2.jpg') }}" alt="">
                    <h4>Mark Johnson</h4>
                    <p>Phasellus porta. Fusce suscipit varius mi. Cum sociis oque.</p>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page2_pic3.jpg') }}" alt="">
                    <h4>David Austin</h4>
                    <p>Phasellus porta. Fusce suscipit varius mi. Cum sociis oque.</p>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page2_pic4.jpg') }}" alt="">
                    <h4>Patrick Pool</h4>
                    <p>Phasellus porta. Fusce suscipit varius mi. Cum sociis oque.</p>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page2_pic5.jpg') }}" alt="">
                    <h4>Thomas Bishop</h4>
                    <p>Phasellus porta. Fusce suscipit varius mi. Cum sociis oque.</p>
                </div>
            </div>

            <hr>

            <h2>
                <span>Cras iaculis magna at pellent</span>
                WHY JOIN US?
            </h2>


            <div class="row marTop1">
                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <img src="{{ asset('assets/images/page2_pic6.jpg') }}" alt="">
                    <h4>Nunc sit amet fringilla dui, eumass</h4>
                    <p>Quam, et faucibus risus tempor in. Suspendisse placerat augue convallis, facilisis ultrices mauris suscipit. Nulla sit amet gravida quam.</p>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <img src="{{ asset('assets/images/page2_pic7.jpg') }}" alt="">
                    <h4>Nunc sit amet fringilla dui, eumass</h4>
                    <p>Quam, et faucibus risus tempor in. Suspendisse placerat augue convallis, facilisis ultrices mauris suscipit. Nulla sit amet gravida quam.</p>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <img src="{{ asset('assets/images/page2_pic8.jpg') }}" alt="">
                    <h4>Nunc sit amet fringilla dui, eumass</h4>
                    <p>Quam, et faucibus risus tempor in. Suspendisse placerat augue convallis, facilisis ultrices mauris suscipit. Nulla sit amet gravida quam.</p>
                </div>

            </div>

        </div>

    </section>

</main>

@endsection