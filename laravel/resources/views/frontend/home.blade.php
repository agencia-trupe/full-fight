@extends('frontend.common.template')

@section('content')

<main>

    <section class="camera_container">
        <div id="camera" class="camera_wrap">
            <div data-src="{{ asset('assets/images/page-1_slide01.jpg') }}">
                <div class="camera_caption fadeIn">
                    <div class="container">
                        <h3>Freestyle Kickboxing</h3>
                        <p>Praesent vestibulum molestie lanean</p>
                    </div>
                </div>
            </div>
            <div data-src="{{ asset('assets/images/page-1_slide02.jpg') }}">
                <div class="camera_caption fadeIn">
                    <div class="container">
                        <h3>Freestyle Kickboxing</h3>
                        <p>Aenean nonummy mauris porta</p>
                    </div>
                </div>
            </div>
            <div data-src="{{ asset('assets/images/page-1_slide03.jpg') }}">
                <div class="camera_caption fadeIn">
                    <div class="container">
                        <h3>Freestyle Kickboxing</h3>
                        <p>Mau a lacuestibulum libero</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="well">
        <div class="container">

            <h2>
                <span>a few words about us</span>
                welcome
            </h2>
            <div class="row">
                <div class="col-lg-8 col-md-12   wow fadeIn col-lg-offset-2">
                    <p  class="cntr">Try Kickboxing at our club for fitness or competition. We have entry level classes that are safe and enjoyable, and will teach you the skills you need to progress into one of our more challenging advanced kickboxing classes. So no matter what your age, come along to our friendly classes and receive expert tuition. </p>
                </div>
            </div>

            <hr>

            <h2  class="mod1">
                <span>Recently added photos</span>
                gallery
            </h2>

            <div class="gall1">
                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>
            </div>
            <div class="wrapper cntr">
                <a href="#" class="btn small">View all gallery</a>
            </div>

            <hr>

            <h2>
                <span>latest news and events</span>
                news
            </h2>


            <div class="row article-section mobile_cntr">
                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <article class="article1">
                        <img src="{{ asset('assets/images/page1_pic5.jpg') }}" alt="">
                        <time datetime="2015">
                            <span class="day">15</span>
                            <span class="month">aug</span>
                        </time>
                        <h4>Competition results from Dublin</h4>
                        <p>We brought down 8 competitors with 1 other student coming along supporting the team...</p>
                        <a href="#" class="btn">Read more</a>
                    </article>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <article class="article1">
                        <img src="{{ asset('assets/images/page1_pic6.jpg') }}" alt="">
                        <time datetime="2015">
                            <span class="day">08</span>
                            <span class="month">aug</span>
                        </time>
                        <h4>Student grading nights</h4>
                        <p>Сongratulate all its junior female and one junior male students on successfully completing...</p>
                        <a href="#" class="btn">Read more</a>
                    </article>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <article class="article1">
                        <img src="{{ asset('assets/images/page1_pic7.jpg') }}" alt="">
                        <time datetime="2015">
                            <span class="day">30</span>
                            <span class="month">aug</span>
                        </time>
                        <h4>Student of the month</h4>
                        <p>Сongratulate Glenn on being awarded student of the month which is due to his hard training...</p>
                        <a href="#" class="btn">Read more</a>
                    </article>
                </div>
            </div>


            <hr>

            <h2>
                <span>Event Calendar</span>
                events
            </h2>

            <p class="owl-title"></p>

            <div class="owl-carousel">
                <div class="item">
                    <table class="table-1">
                        <tr>
                            <td class="col-1">9/23 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">Grapple Strike Takedown Launch</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 100%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">9/25 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">New GSTMA Rashguard</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 100%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">9/27 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">Charity Fun day!</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 60%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">10/06 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">GFKC Series Tournament Dates 2015!</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 100%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">10/05 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">Troy Chalkley’s Sport relief challenge!</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 80%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">9/23 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">Skill Badge Exams!</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 100%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">9/23 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">Northampton Interclub </td>
                            <td class="col-4"><span class="stars_main"><span style="width: 50%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">9/23 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">Bristol Fight Night</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 100%"></span></span></td>
                        </tr>
                    </table>
                </div>


                <div class="item">
                    <table class="table-1">

                        <tr>
                            <td class="col-1">10/06 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">GFKC Series Tournament Dates 2015!</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 100%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">10/05 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">Troy Chalkley’s Sport relief challenge!</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 80%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">9/23 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">Grapple Strike Takedown Launch</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 100%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">9/25 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">New GSTMA Rashguard</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 100%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">9/27 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">Charity Fun day!</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 60%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">9/23 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">Skill Badge Exams!</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 100%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">9/23 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">Northampton Interclub </td>
                            <td class="col-4"><span class="stars_main"><span style="width: 50%"></span></span></td>
                        </tr>

                        <tr>
                            <td class="col-1">9/23 </td>
                            <td class="col-2">9:00 PM</td>
                            <td class="col-3">Bristol Fight Night</td>
                            <td class="col-4"><span class="stars_main"><span style="width: 100%"></span></span></td>
                        </tr>
                    </table>
                </div>
            </div>

            <hr>

            <h2>
                <span>Strike team</span>
                fighters
            </h2>

            <div class="row marTop1 mobile_cntr">
                <div class="col-md-3 col-sm-3 col-xs-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page1_pic8.jpg') }}" alt="">
                    <h4>Tom James</h4>
                    <p>Freestyle Kickboxing <br>3rd  Degree Black belt</p>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page1_pic9.jpg') }}" alt="">
                    <h4>Sam Kromstain</h4>
                    <p>Freestyle Kickboxing <br>3rd  Degree Black belt</p>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page1_pic10.jpg') }}" alt="">
                    <h4>Laura Stegner</h4>
                    <p>Freestyle Kickboxing <br>1st  Degree Black belt</p>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page1_pic11.jpg') }}" alt="">
                    <h4>Albert Frost</h4>
                    <p>Freestyle Kickboxing <br>1st  Degree Black belt</p>
                </div>
            </div>

            <div class="row mobile_cntr">
                <div class="col-md-3 col-sm-3 col-xs-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page1_pic12.jpg') }}" alt="">
                    <h4>Helen Tompson</h4>
                    <p>Freestyle Kickboxing <br>2nd  Degree Black belt </p>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page1_pic13.jpg') }}" alt="">
                    <h4>Robert Smith</h4>
                    <p>Freestyle Kickboxing <br>3rd  Degree Black belt </p>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page1_pic14.jpg') }}" alt="">
                    <h4>Hugo Star</h4>
                    <p>Freestyle Kickboxing <br>1st  Degree Black belt </p>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6  wow fadeIn">
                    <img src="{{ asset('assets/images/page1_pic15.jpg') }}" alt="">
                    <h4>Caroline Beek</h4>
                    <p>Freestyle Kickboxing <br>3rd  Degree Black belt </p>
                </div>
            </div>


            <hr>

            <h2>
                <span>What we are proud of</span>
                numbers
            </h2>

            <div class="row">
                <ul class="list1">
                    <li class="col-md-3 col-sm-3 col-xs-6  wow fadeIn material-icons-change_history">
                        <p class="num">128</p>
                        <h4>Victories</h4>
                        <p>A wide variety <br>of fighting styles</p>
                    </li>

                    <li class="col-md-3 col-sm-3 col-xs-6  wow fadeIn material-icons-close">
                        <p class="num">256</p>
                        <h4>Halls</h4>
                        <p>Welcome to controlled <br>safe environment </p>
                    </li>

                    <li class="col-md-3 col-sm-3 col-xs-6  wow fadeIn material-icons-clear_all">
                        <p class="num">512</p>
                        <h4>Fighters</h4>
                        <p>We teach all aspects <br>of kickboxing  </p>
                    </li>

                    <li class="col-md-3 col-sm-3 col-xs-6  wow fadeIn material-icons-check_box_outline_blank">
                        <p class="num">1024</p>
                        <h4>Battles</h4>
                        <p>Be prepared for any <br>situation in your life </p>
                    </li>
                </ul>
            </div>

            <hr>

            <h2>
                <span>contact us</span>
                get in touch
            </h2>

            <form method="post" action="bat/rd-mailform.php" class="mailform">
                <input type="hidden" name="form-type" value="contact">
                <fieldset class="row">
                    <label class="col-md-6 col-sm-12 col-xs-12">
                        <input type="text" name="name" placeholder="Name:" data-constraints="@LettersOnly @NotEmpty">
                    </label>
                    <label class="col-md-6 col-sm-12 col-xs-12">
                        <input type="text" name="email" placeholder="Email:" data-constraints="@Email @NotEmpty">
                    </label>
                    <label class="col-md-12 col-sm-12 col-xs-12">
                        <textarea name="message" placeholder="Message:" data-constraints="@NotEmpty"></textarea>
                    </label>
                    <div class="mfControls col-md-12 col-sm-12 col-xs-12">
                        <button type="submit" class="btn">Send it</button>
                    </div>
                </fieldset>
            </form>

            <hr>
            <h2>
                <span>our address</span>
                Contacts
            </h2>
        </div>

        <section class="map">
            <div id="google-map" class="map_model"></div>
            <ul class="map_locations">
                <li data-x="-73.9874068" data-y="40.643180">
                    <p> 9870 St Vincent Place, Glasgow, DC 45 Fr 45. <span>800 2345-6789</span></p>
                </li>
            </ul>
        </section>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <ul class="list2">
                    <li class="col-md-4 col-sm-4 col-xs-4  wow fadeIn material-icons-home">
                        <h4>address</h4>
                        <address>9863 - 9867 Mill Road, <br>Cambridge, MG09 99HT </address>
                    </li>

                    <li class="col-md-4 col-sm-4 col-xs-4  wow fadeIn material-icons-local_phone">
                        <h4>phone</h4>
                        <a href="callto:#">+1 800 603 6035</a>
                    </li>

                    <li class="col-md-4 col-sm-4 col-xs-4  wow fadeIn material-icons-laptop">
                        <h4>mail</h4>
                        <a href="mailto:#">mail@demolink.org</a>
                    </li>


                </ul>
            </div>
        </div>
    </section>

</main>

@endsection
