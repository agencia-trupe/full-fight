@extends('frontend.common.template')

@section('content')

<main>

    <section class="well">
        <div class="container">
            <h2>
                <span>21/08/2016</span>
                Lorem ipsum dolor sit amet, consectetur.
            </h2>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12  wow fadeIn" style="text-align:center">
                    <img src="{{ asset('assets/images/page2_pic1.jpg') }}" alt="">
                </div>

                <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12  wow fadeIn">
                    <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto laudantium soluta iure perferendis corporis quaerat, tempora dolore doloremque quisquam commodi.</h4>
                    <p>Duis ac turpis. Integer rutrum ante eu lacus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aennummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis oque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at.</p>
                    <p>Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. Lorem ipsum dolor sit amet, consectetue.</p>
                    <p>Duis ac turpis. Integer rutrum ante eu lacus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aennummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis oque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at.</p>
                    <p>Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. Lorem ipsum dolor sit amet, consectetue.</p>
                    <p>Duis ac turpis. Integer rutrum ante eu lacus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aennummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis oque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at.</p>
                    <p>Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus. Lorem ipsum dolor sit amet, consectetue.</p>

                    <a href="#" class="btn pull-left" style="margin-top:40px">Voltar</a>
                </div>
            </div>
        </div>
    </section>

</main>

@endsection