@extends('frontend.common.template')

@section('content')

<main>

    <section class="well">
        <div class="container">

            <h2>
                <span>Fusce dictum sit amet turpis</span>
                Latest news
            </h2>

            <div class="row article-section mobile_cntr">
                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <article class="article1">
                        <img src="{{ asset('assets/images/page4_pic1.jpg') }}" alt="">
                        <time datetime="2015">
                            <span class="day">21</span>
                            <span class="month">aug</span>
                        </time>
                        <h4>Nunc sit amet fringill ringilla amet</h4>
                        <p>Deis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                        <a href="{{ route('news.show') }}" class="btn">Read more</a>
                    </article>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <article class="article1">
                        <img src="{{ asset('assets/images/page4_pic2.jpg') }}" alt="">
                        <time datetime="2015">
                            <span class="day">28</span>
                            <span class="month">aug</span>
                        </time>
                        <h4>Nunc sit amet fringill ringilla amet</h4>
                        <p>Deis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                        <a href="{{ route('news.show') }}" class="btn">Read more</a>
                    </article>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <article class="article1">
                        <img src="{{ asset('assets/images/page4_pic3.jpg') }}" alt="">
                        <time datetime="2015">
                            <span class="day">02</span>
                            <span class="month">sep</span>
                        </time>
                        <h4>Nunc sit amet fringill ringilla amet</h4>
                        <p>Deis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                        <a href="{{ route('news.show') }}" class="btn">Read more</a>
                    </article>
                </div>
            </div>

            <div class="row article-section mobile_cntr">
                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <article class="article1">
                        <img src="{{ asset('assets/images/page4_pic1.jpg') }}" alt="">
                        <time datetime="2015">
                            <span class="day">21</span>
                            <span class="month">aug</span>
                        </time>
                        <h4>Nunc sit amet fringill ringilla amet</h4>
                        <p>Deis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                        <a href="{{ route('news.show') }}" class="btn">Read more</a>
                    </article>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <article class="article1">
                        <img src="{{ asset('assets/images/page4_pic2.jpg') }}" alt="">
                        <time datetime="2015">
                            <span class="day">28</span>
                            <span class="month">aug</span>
                        </time>
                        <h4>Nunc sit amet fringill ringilla amet</h4>
                        <p>Deis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                        <a href="{{ route('news.show') }}" class="btn">Read more</a>
                    </article>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <article class="article1">
                        <img src="{{ asset('assets/images/page4_pic3.jpg') }}" alt="">
                        <time datetime="2015">
                            <span class="day">02</span>
                            <span class="month">sep</span>
                        </time>
                        <h4>Nunc sit amet fringill ringilla amet</h4>
                        <p>Deis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                        <a href="{{ route('news.show') }}" class="btn">Read more</a>
                    </article>
                </div>
            </div>

            <div class="row article-section mobile_cntr">
                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <article class="article1">
                        <img src="{{ asset('assets/images/page4_pic1.jpg') }}" alt="">
                        <time datetime="2015">
                            <span class="day">21</span>
                            <span class="month">aug</span>
                        </time>
                        <h4>Nunc sit amet fringill ringilla amet</h4>
                        <p>Deis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                        <a href="{{ route('news.show') }}" class="btn">Read more</a>
                    </article>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <article class="article1">
                        <img src="{{ asset('assets/images/page4_pic2.jpg') }}" alt="">
                        <time datetime="2015">
                            <span class="day">28</span>
                            <span class="month">aug</span>
                        </time>
                        <h4>Nunc sit amet fringill ringilla amet</h4>
                        <p>Deis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                        <a href="{{ route('news.show') }}" class="btn">Read more</a>
                    </article>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12  wow fadeIn">
                    <article class="article1">
                        <img src="{{ asset('assets/images/page4_pic3.jpg') }}" alt="">
                        <time datetime="2015">
                            <span class="day">02</span>
                            <span class="month">sep</span>
                        </time>
                        <h4>Nunc sit amet fringill ringilla amet</h4>
                        <p>Deis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                        <a href="{{ route('news.show') }}" class="btn">Read more</a>
                    </article>
                </div>
            </div>

            <hr>

            <a href="#" class="btn pull-left">Anteriores</a>
            <a href="#" class="btn pull-right">Próximas</a>

        </div>

    </section>

</main>

@endsection