@extends('frontend.common.template')

@section('content')

<main>

    <section class="well2">
        <div class="container">

            <h2>
                <span>Phasellus tristique sem eget</span>
                main services
            </h2>

            <ul class="row index-list">
                <li class="col-md-6 col-sm-6 col-xs-12  wow fadeIn material-icons-looks_one">
                    <h4>Donec sit amet eros lorem ipsum dolor sit ame</h4>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesua...</p>
                </li>

                <li class="col-md-6 col-sm-6 col-xs-12  wow fadeIn material-icons-looks_two">
                    <h4>Integer rutrum ante eu lacus lorem ipsum</h4>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesua...</p>
                </li>

                <li class="col-md-6 col-sm-6 col-xs-12  wow fadeIn material-icons-looks_3">
                    <h4>Lorem ipsum dolor sit amet, consectetuer adipi</h4>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesua...</p>
                </li>

                <li class="col-md-6 col-sm-6 col-xs-12  wow fadeIn material-icons-looks_4">
                    <h4>Fusce feugiat malesuada odio morbi nunc odio</h4>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesua...</p>
                </li>

            </ul>


            <hr>

            <h2>
                <span>Lorem ipsum dolor sit amet</span>
                our Classes
            </h2>

            <div class="row marTop1  mobile_cntr">
                <div class="col-md-4 col-sm-4 col-xs-4  wow fadeIn">
                    <img src="{{ asset('assets/images/page3_pic1.jpg') }}" alt="">
                    <h4>Circuit training</h4>
                    <p>Quam, et faucibus risus tempor in. Suspendisse placerat augue convallis, facilisis ultrice.</p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4  wow fadeIn">
                    <img src="{{ asset('assets/images/page3_pic2.jpg') }}" alt="">
                    <h4>Little ninjas</h4>
                    <p>Quam, et faucibus risus tempor in. Suspendisse placerat augue convallis, facilisis ultrice.</p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4  wow fadeIn">
                    <img src="{{ asset('assets/images/page3_pic3.jpg') }}" alt="">
                    <h4>Beginner classes</h4>
                    <p>Quam, et faucibus risus tempor in. Suspendisse placerat augue convallis, facilisis ultrice.</p>
                </div>
            </div>

            <div class="row  mobile_cntr">
                <div class="col-md-4 col-sm-4 col-xs-4  wow fadeIn">
                    <img src="{{ asset('assets/images/page3_pic4.jpg') }}" alt="">
                    <h4>Featured technique</h4>
                    <p>Quam, et faucibus risus tempor in. Suspendisse placerat augue convallis, facilisis ultrice.</p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4  wow fadeIn">
                    <img src="{{ asset('assets/images/page3_pic5.jpg') }}" alt="">
                    <h4>Adult classes</h4>
                    <p>Quam, et faucibus risus tempor in. Suspendisse placerat augue convallis, facilisis ultrice.</p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4  wow fadeIn">
                    <img src="{{ asset('assets/images/page3_pic6.jpg') }}" alt="">
                    <h4>Ladies classes</h4>
                    <p>Quam, et faucibus risus tempor in. Suspendisse placerat augue convallis, facilisis ultrice.</p>
                </div>
            </div>

            <hr class="marTop3">

            <h2>
                <span>nteger ut lacus sit amen</span>
                Self defence seminars
            </h2>

            <ul class="list3">
                <li class="col-md-4 col-sm-4 col-xs-12  wow fadeIn material-icons-people">
                    <h4>Vivamus posu ultrici</h4>
                    <p>Donec vitae ante nec nisl blandit lobortis ut eget orci. Nunc sit amet fringilla dui, eu eleme ntum enim. Maecenas sed ornare nisi.</p>
                </li>

                <li class="col-md-4 col-sm-4 col-xs-12  wow fadeIn material-icons-accessibility">
                    <h4>Curabitur consequat turp</h4>
                    <p>Donec vitae ante nec nisl blandit lobortis ut eget orci. Nunc sit amet fringilla dui, eu eleme ntum enim. Maecenas sed ornare nisi.</p>
                </li>

                <li class="col-md-4 col-sm-4 col-xs-12  wow fadeIn material-icons-favorite">
                    <h4>Proin enim metu</h4>
                    <p>Donec vitae ante nec nisl blandit lobortis ut eget orci. Nunc sit amet fringilla dui, eu eleme ntum enim. Maecenas sed ornare nisi.</p>
                </li>
            </ul>

        </div>

    </section>

</main>

@endsection