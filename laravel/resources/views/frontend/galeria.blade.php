@extends('frontend.common.template')

@section('content')

<main>

    <section class="well">
        <div class="container">

             <h2>
                <span>Luptaum zzelenit augue duis</span>
                featured photos
            </h2>

            <div class="gall1 mod1">
                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>

                <div class="col1 wow fadeIn">
                    <a class="thumb" href="{{ asset('assets/images/page5_pic11.jpg') }}" data-fancybox-group='1'>
                        <img src="{{ asset('assets/images/page5_pic11.jpg') }}" alt=""/>
                        <span class="thumb_overlay"></span>
                    </a>
                </div>
            </div>

        </div>

    </section>

</main>

@endsection