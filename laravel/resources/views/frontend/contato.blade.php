@extends('frontend.common.template')

@section('content')

<main>

    <section class="well">
        <div class="container">

            <h2>
                <span>stibulum liber nisl</span>
                get in touch
            </h2>

            <form method="post" action="bat/rd-mailform.php" class="mailform">
                <input type="hidden" name="form-type" value="contact">
                <fieldset class="row">
                    <label class="col-md-6 col-sm-12 col-xs-12">
                        <input type="text" name="name" placeholder="Name:" data-constraints="@LettersOnly @NotEmpty">
                    </label>
                    <label class="col-md-6 col-sm-12 col-xs-12">
                        <input type="text" name="email" placeholder="Email:" data-constraints="@Email @NotEmpty">
                    </label>
                    <label class="col-md-12 col-sm-12 col-xs-12">
                        <textarea name="message" placeholder="Message:" data-constraints="@NotEmpty"></textarea>
                    </label>
                    <div class="mfControls col-md-12 col-sm-12 col-xs-12">
                        <button type="submit" class="btn">Send it</button>
                    </div>
                </fieldset>
            </form>

            <hr>

            <h2>
                <span>stibulum liber nisl</span>
                Contacts
            </h2>
        </div>

        <section class="map">
            <div id="google-map" class="map_model"></div>
            <ul class="map_locations">
                <li data-x="-73.9874068" data-y="40.643180">
                    <p> 9870 St Vincent Place, Glasgow, DC 45 Fr 45. <span>800 2345-6789</span></p>
                </li>
            </ul>
        </section>

    </section>

    <section>
        <div class="container">
            <div class="row">
                <ul class="list2">
                    <li class="col-md-4 col-sm-4 col-xs-4  wow fadeIn material-icons-home">
                        <h4>address</h4>
                        <address>9863 - 9867 Mill Road, <br>Cambridge, MG09 99HT </address>
                    </li>

                    <li class="col-md-4 col-sm-4 col-xs-4  wow fadeIn material-icons-local_phone">
                        <h4>phone</h4>
                        <a href="callto:#">+1 800 603 6035</a>
                    </li>

                    <li class="col-md-4 col-sm-4 col-xs-4  wow fadeIn material-icons-laptop">
                        <h4>mail</h4>
                        <a href="mailto:#">mail@demolink.org</a>
                    </li>

                </ul>
            </div>
        </div>
    </section>

</main>

@endsection