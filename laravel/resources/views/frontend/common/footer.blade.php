<footer>
    <div class="container">
        @unless(Route::currentRouteName() == 'home') <hr> @endif
        Full Fight  © <span id="copyright-year"></span> • <a href="http://trupe.net" target="_blank">Criação de Sites</a>: <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a>
    </div>
</footer>