<header @if(Route::currentRouteName() == 'home') class="camera-header" @endif>
    <div id="stuck_container" class="stuck_container">
        <div class="brand">
            <h1 class="brand_name">
                <a href="{{ route('home') }}">F<span>F</span></a>
            </h1>
            <p class="brand_slogan">
            Full Fight
            </p>
        </div>

        <nav class="nav">
            <ul class="sf-menu" data-type="navbar">
                <li @if(Route::currentRouteName() == 'home') class="active" @endif>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li @if(Route::currentRouteName() == 'academia') class="active" @endif>
                    <a href="{{ route('academia') }}">Academia</a>
                </li>
                <li @if(Route::currentRouteName() == 'modalidades') class="active" @endif>
                    <a href="{{ route('modalidades') }}">Modalidades</a>
                </li>
                <li @if(str_is('news*', Route::currentRouteName())) class="active" @endif>
                    <a href="{{ route('news') }}">News</a>
                </li>
                <li @if(Route::currentRouteName() == 'galeria') class="active" @endif>
                    <a href="{{ route('galeria') }}">Galeria</a>
                </li>
                <li @if(Route::currentRouteName() == 'contato') class="active" @endif>
                    <a href="{{ route('contato') }}">Contato</a>
                </li>
            </ul>
        </nav>
    </div>

    @if(Route::currentRouteName() == 'home')
    <div class="wrapper header-txt">
        <div class="header-txt-all">
            <p class="header-txt1">controlled</p>
            <img src="{{ asset('assets/images/home-obj.png') }}" alt="">
            <p class="header-txt2">environment</p>
        </div>
    </div>

    <ul class="inline-list">
        <li><a href="#" class="fa-twitter"></a></li>
        <li><a href="#" class="fa-facebook"></a></li>
        <li><a href="#" class="fa-pinterest"></a></li>
        <li><a href="#" class="fa-youtube"></a></li>
        <li><a href="#" class="fa-instagram"></a></li>
    </ul>
    @endif
</header>
