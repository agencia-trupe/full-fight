@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Lutador</h2>
    </legend>

    {!! Form::model($lutador, [
        'route'  => ['painel.lutadores.update', $lutador->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.lutadores.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
