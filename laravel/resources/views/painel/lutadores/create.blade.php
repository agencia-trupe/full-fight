@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Adicionar Lutador</h2>
    </legend>

    {!! Form::open(['route' => 'painel.lutadores.store', 'files' => true]) !!}

        @include('painel.lutadores.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
