@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Lutadores
            <a href="{{ route('painel.lutadores.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Lutador</a>
        </h2>
    </legend>

    @if(!count($lutadores))
    <div class="alert alert-warning" role="alert">Nenhum lutador cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="lutadores">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Imagem</th>
                <th>Nome</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($lutadores as $lutador)
            <tr class="tr-row" id="{{ $lutador->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><img src="{{ url('assets/img/lutadores/'.$lutador->imagem) }}" alt="" style="width:100%;max-width:200px;height:auto;"></td>
                <td>{{ $lutador->nome }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.lutadores.destroy', $lutador->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.lutadores.edit', $lutador->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
