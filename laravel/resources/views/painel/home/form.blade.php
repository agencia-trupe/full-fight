@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_principal', 'Texto Principal') !!}
    {!! Form::textarea('texto_principal', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">
    <div class="form-group col-md-2">
        {!! Form::label('numero1', 'Números (1)') !!}
        {!! Form::text('numero1', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-5">
        {!! Form::label('numero1_titulo', 'Números (1) - Título') !!}
        {!! Form::text('numero1_titulo', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-5">
        {!! Form::label('numero1_frase', 'Números (1) - Frase') !!}
        {!! Form::text('numero1_frase', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-2">
        {!! Form::label('numero2', 'Números (2)') !!}
        {!! Form::text('numero2', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-5">
        {!! Form::label('numero2_titulo', 'Números (2) - Título') !!}
        {!! Form::text('numero2_titulo', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-5">
        {!! Form::label('numero2_frase', 'Números (2) - Frase') !!}
        {!! Form::text('numero2_frase', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-2">
        {!! Form::label('numero3', 'Números (3)') !!}
        {!! Form::text('numero3', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-5">
        {!! Form::label('numero3_titulo', 'Números (3) - Título') !!}
        {!! Form::text('numero3_titulo', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-5">
        {!! Form::label('numero3_frase', 'Números (3) - Frase') !!}
        {!! Form::text('numero3_frase', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-2">
        {!! Form::label('numero4', 'Números (4)') !!}
        {!! Form::text('numero4', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-5">
        {!! Form::label('numero4_titulo', 'Números (4) - Título') !!}
        {!! Form::text('numero4_titulo', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-5">
        {!! Form::label('numero4_frase', 'Números (4) - Frase') !!}
        {!! Form::text('numero4_frase', null, ['class' => 'form-control']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
