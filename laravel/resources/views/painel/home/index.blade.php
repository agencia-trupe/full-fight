@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Home</h2>
    </legend>

    {!! Form::model($home, [
        'route'  => ['painel.home.update', $home->id],
        'method' => 'patch'])
    !!}

    @include('painel.home.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
